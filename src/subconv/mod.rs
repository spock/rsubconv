
use regex::bytes::Regex;
// extern crate regex;
// use regex::bytes::Regex;
use std::str;
const MPL_REGEXP: &'static str = r"\[(?P<start>\d+)\]\[(?P<stop>\d+)\](?P<text>.*)";

// enum SubtitleFormat {
//     MPL2(String),
//     SRT(String),
// }
#[allow(dead_code)]
fn ms_to_srt(val: &u32) -> String {
    let hour: u32 = val / 36000;
    let minutes: u32 = (val % 36000) / 600;
    let seconds: u32 = ((val % 36000) % 600) / 10;
    let mills: u32 = ((val % 36000) % 600) % 10;
    let result = format!("{:02}:{:02}:{:02},{:03}", hour, minutes, seconds, mills);
    return result;

}


pub fn decode_mpl_to_srt(src: &Vec<u8>, dst: &mut String) -> Option<i32> {
    let mut cnt: i32 = 0;
    let re = Regex::new(MPL_REGEXP).unwrap();
    // START: Ten kod idzie do biblioteki
    // let mut out_string: String = String::with_capacity(10000);
    for (i, e) in re.captures_iter(src.as_slice()).enumerate() {
        let start: u32 = match str::from_utf8(e.name("start").unwrap()) {
            Ok(start) => start.parse::<u32>().unwrap(),
            Err(e) => {
                println!("Matching start frame failed {}", e);
                continue;
            }
        };
        let stop: u32 = match str::from_utf8(e.name("stop").unwrap()) {
            Ok(stop) => stop.parse::<u32>().expect("Unable to decode number from stop string !"),
            Err(e) => {
                println!("Matchig end frame failed {}", e);
                continue;
            }
        };
        // Pracujemy na ciągu który czesto nie jest UTF'em (np: CP1250)
        // TODO: Użyć wrapera do iconv i spróbować na konwersji do utf8 a potem bezpiecznie
        //          na tym pracować.
        let text = unsafe { str::from_utf8_unchecked(e.name("text").unwrap()) };
        dst.push_str(&format!("{}\n{} --> {}\n{}\n\n",
                              i,
                              ms_to_srt(&start),
                              ms_to_srt(&stop),
                              text));
        cnt += 1;
    }
    Some(cnt)
}
