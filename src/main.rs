extern crate regex;

// use std::io;
use std::io::prelude::*;
use std::fs::File;
use std::env;
use std::process;
mod subconv;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");



macro_rules! println_stderr(
    ($($arg:tt)*) => { {
        let r = writeln!(&mut ::std::io::stderr(), $($arg)*);
        r.expect("failed printing to stderr");
    } }
);

fn debug(msg: &str) {
    //let _ = std::io::stderr().write(msg.as_bytes());
	println_stderr!("{}", msg);
}
fn help(progname: &str) {
    println!("{0} {1}
Program do konwersji napisów z formatu MPL2 do SRT. (spock@omegastar.eu)
Użycie: {0} <source_file> [<dest_file>]
<source_file> - plik źródłowy w formacie MPL2.
<dest_file> - opcjonalnie nazwa pliku wyjściowego. Jeśli nie podana - nadpisz oryginalny plik.
",
             progname,VERSION);
}
fn main() {
    let args: Vec<_> = env::args().collect();
    let args_count = args.len();
    if args_count < 2 {
        help(&args[0]);
        process::exit(1);
    }
    let out_filename = match args.len() {
        2 => &args[1],
        3 => &args[2],
        _ => panic!("To many params !"),
    };
    let src_path = &args[1];
    let mut s = vec![0;10];
    // scope for opened file
    {
        let mut f = File::open(src_path).expect(&format!("Unable to open {}", src_path));// {
        match f.read_to_end(&mut s) {
            Ok(r) => r,
            Err(e) => panic!("Can read file !: {}", e),
        };
    };
    // let mut cnt: u32 = 0;
    let mut out_string = String::with_capacity(10000);
    let cnt = subconv::decode_mpl_to_srt(&s, &mut out_string);
    match cnt {
    Some(cnt) => {
        // zakładamy, że jak przeparsowaliśmy 10 lini to plik był ok
        // println!("{}",out_string);
    if cnt > 10 {
        let mut fs_out = File::create(out_filename)
                             .expect(&format!("Unable to create file {}", out_filename));
        match fs_out.write_all(out_string.as_bytes()) {
            Ok(_) => debug(&format!("OK {} lines processed", cnt)),
            Err(e) => panic!("NG Unable to write to output file {}", e),
        };
} else {
    println_stderr!("NG {} lines decoded - Are you sure source file is in MPL2 format ?\n", cnt);
}
},
    None => println_stderr!("NG Unable to decode enought lines to consider file as MPL2 format")
}
}
