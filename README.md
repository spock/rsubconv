# rsubconv
---
This program is simple, fast subtitle converter from MPL2 format to srt (subrip) supported by my Samsung TV :)
Its purpose is to be called from other scripts.

MPL2 Specification

Time indexes in MPL2 format are in square brackets:

    [123][456] Sample subtitle

Where the first number is the time when the subtitle    
appears, and the  
second number is the time when the subtitle disappears.
The time is  given in decaseconds (0,1 s). [123][456] means that the subtitle  appears in 12,3 second of the film, and disappears in 45,6 second.

---
## Usage
    rsubconv <mpl2_subtitle_file> [<optional_dest_file>]

If you will not provide optional_dest_file original file will be overwritten with conversion result.
If program will not find more than 10 proper MPL2 lines it will not process result and no file will be written.
---
Nothing less nothing more.
